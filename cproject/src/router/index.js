/**
 * createRouter 创建router 实例对象
 * createWebHistory 创建history模式的路由
 * routes 里面配置path和component对应关系的位置
 * */


import {createRouter, createWebHistory} from 'vue-router'
// import HomeView from '../views/HomeView.vue'
import Layout from '@/views/Layout/index.vue'
import Login from '@/views/Login/index.vue'
import Category from '@/views/Category/index.vue'
import Home from '@/views/Home/index.vue'
const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'Layout',
            component: Layout,
            children: [
                {
                    path:"",
                    component: Home,
                },
                {
                    path:"catrgory",
                    component: Category,
                },
            ],
        },
        {
            path: '/Login',
            name: 'Login',
            component: Login
        }

    ]
})

export default router
