import '@/styles/common.scss'
import { createApp } from 'vue'
import { createPinia } from 'pinia'

// import App from './App.vue'
import AppView from './AppView.vue'
import router from './router'

const app = createApp(AppView)

app.use(createPinia())
app.use(router)

app.mount('#app')
